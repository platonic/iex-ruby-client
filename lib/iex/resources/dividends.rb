module IEX
  module Resources
    class Dividends < Resource
      property 'exDate'
      property 'paymentDate'
      property 'recordDate'
      property 'declaredDate'
      property 'amount'
      property 'flag'
      property 'type'
      property 'qualified'
      property 'indicated'

      def self.get(symbol, range = nil)
        IEX::Api::Dividends.get(symbol, range).map do |data|
          new data
        end
      rescue Faraday::ResourceNotFound => e
        raise IEX::Errors::SymbolNotFoundError.new(symbol, e.response[:body])
      end
    end
  end
end
