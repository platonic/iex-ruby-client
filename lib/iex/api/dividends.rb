module IEX
  module Api
    module Dividends
      def self.get(symbol, range)
        connection(symbol, range).get.body
      end

      def self.connection(symbol, range)
        IEX::Api.default_connection [
          symbol,
          'dividends',
          range
        ].compact.join('/')
      end
    end
  end
end
